package lu.aqu.metrognome

import android.content.Context
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.view.SoundEffectConstants
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import lu.aqu.metrognome.databinding.ActivityMainBinding
import lu.aqu.metrognome.utils.InjectorUtils
import lu.aqu.metrognome.viewmodels.MetronomeViewModel


class MainActivity : AppCompatActivity() {

    private lateinit var mModel: MetronomeViewModel
    private lateinit var mAudioManager: AudioManager
    private lateinit var mBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val factory = InjectorUtils.provideMetronomeViewModelFactory(this)
        mModel = ViewModelProviders.of(this, factory).get(MetronomeViewModel::class.java)
        mAudioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager

        registerObservers()
        initView()
    }

    private fun registerObservers() {
        mModel.beat.observe(this, Observer<Int> { newBeat ->
            mBinding.measure.setStep(newBeat + 1)

            val volume = if (newBeat == 0) 1f else 0.3f
            mAudioManager.playSoundEffect(SoundEffectConstants.CLICK, volume)
        })

        mModel.bpm.observe(this, Observer<Int> { bpm ->
            mBinding.txtBpm.text = bpm.toString()
            mBinding.btnIncreaseBpm.isEnabled = bpm < mModel.maxBpm
            mBinding.btnDecreaseBpm.isEnabled = bpm > mModel.minBpm
            mBinding.bpm.progress = bpm
        })

        mModel.beatsPerMeasure.observe(this, Observer<Int> { beatsPerMeasure ->
            mBinding.measure.setBeats(beatsPerMeasure)
        })
    }

    /**
     * initialize onClick / onChange listeners and set default values
     */
    private fun initView() {
        mBinding.apply {
            btnToggle.setOnClickListener(ToggleMetronomeClickListener())

            btnIncreaseBpm.setOnClickListener {
                mModel.setBpmSafe(mModel.getBpmSafe() + 5)
            }

            btnDecreaseBpm.setOnClickListener {
                mModel.setBpmSafe(mModel.getBpmSafe() - 5)
            }

            btnIncreaseBeatsPerMeasure.setOnClickListener {
                mModel.increaseBeatsPerMeasure()
            }

            btnDecreaseBeatsPerMeasure.setOnClickListener {
                mModel.decreaseBeatsPerMeasure()
            }

            bpm.let { seekBar ->
                seekBar.max = mModel.maxBpm
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    seekBar.min = mModel.minBpm
                }
                seekBar.setOnSeekBarChangeListener(BpmChangeListener())
                seekBar.progress = mModel.getBpmSafe()
            }
        }
    }

    inner class ToggleMetronomeClickListener : View.OnClickListener {
        override fun onClick(v: View?) {
            val active = mModel.toggle()

            val imageResource = if (active) {
                R.drawable.ic_stop_white_24dp
            } else {
                R.drawable.ic_play_arrow_white_24dp
            }

            mBinding.apply {
                arrayOf(
                        bpm,
                        btnDecreaseBeatsPerMeasure,
                        btnIncreaseBeatsPerMeasure,
                        btnDecreaseBpm,
                        btnIncreaseBpm
                ).forEach { view ->
                    view.isEnabled = !active
                }

                btnToggle.setImageResource(imageResource)
            }
        }
    }

    inner class BpmChangeListener : SeekBar.OnSeekBarChangeListener {

        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            if (fromUser) {
                mModel.setBpmSafe(progress)
            }
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {
            // do nothing
        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            // do nothing
        }
    }
}
