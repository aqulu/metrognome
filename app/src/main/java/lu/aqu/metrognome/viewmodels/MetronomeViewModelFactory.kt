package lu.aqu.metrognome.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MetronomeViewModelFactory(
        private val minBpm: Int,
        private val maxBpm: Int
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MetronomeViewModel(minBpm, maxBpm) as T
    }
}