package lu.aqu.metrognome.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

class MetronomeViewModel internal constructor(
        val minBpm: Int,
        val maxBpm: Int,
        private val defaultBpm: Int = 120,
        private val defaultBeatsPerMeasure: Int = 4
) : ViewModel() {

    val beat: MutableLiveData<Int> = MutableLiveData<Int>().apply {
        value = 0
    }
    val bpm: MutableLiveData<Int> = MutableLiveData<Int>().apply {
        value = defaultBpm
    }
    val beatsPerMeasure: MutableLiveData<Int> = MutableLiveData<Int>().apply {
        value = defaultBeatsPerMeasure
    }
    private var subscription: Disposable? = null

    /**
     * toggles the state of the metronome and returns the new state
     * @return Boolean whether or not the metronome is currently active
     */
    fun toggle(): Boolean {
        if (isActive()) {
            stop()
        } else {
            start()
        }
        return isActive()
    }

    /**
     * starts the metronome
     */
    private fun start() {
        stop()

        val bpm = this.bpm.value ?: minBpm
        val interval = 60 * 1000 / bpm
        subscription = Observable.interval(interval.toLong(), TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    tick()
                }
    }

    private fun tick() {
        val beats = beatsPerMeasure.value ?: defaultBeatsPerMeasure
        val current = beat.value ?: 0
        beat.value = (current + 1) % beats
    }

    /**
     * stops the metronome, if it is currently active
     */
    private fun stop() {
        if (subscription != null && !subscription!!.isDisposed) {
            subscription?.dispose()
        }
        beat.value = 0
    }

    /**
     * @return Boolean whether or not the metronome is currently active
     */
    private fun isActive(): Boolean {
        return subscription != null && !subscription!!.isDisposed
    }

    fun getBpmSafe(): Int {
        return bpm.value ?: minBpm
    }

    /**
     * sets the bpm for this instance (any value between 1 and 300)
     */
    fun setBpmSafe(bpm: Int) {
        val min = if (minBpm > 0) minBpm else 1
        val max = if (maxBpm > min) maxBpm else min

        this.bpm.value = when {
            bpm < min -> min
            bpm > max -> max
            else -> bpm
        }
    }

    /**
     * increases the beatsPerMeasure variable by 1
     */
    fun increaseBeatsPerMeasure() {
        val currentValue = beatsPerMeasure.value ?: defaultBeatsPerMeasure
        beatsPerMeasure.value = currentValue + 1
    }

    /**
     * decreases beatsPerMeasure ensuring the value does not become smaller than 1
     */
    fun decreaseBeatsPerMeasure() {
        val currentValue = beatsPerMeasure.value ?: defaultBeatsPerMeasure
        if (currentValue > 1) {
            beatsPerMeasure.value = currentValue - 1
        }
    }
}
