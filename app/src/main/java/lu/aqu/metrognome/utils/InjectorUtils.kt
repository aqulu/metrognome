package lu.aqu.metrognome.utils

import android.content.Context
import lu.aqu.metrognome.R
import lu.aqu.metrognome.viewmodels.MetronomeViewModelFactory

object InjectorUtils {
    fun provideMetronomeViewModelFactory(context: Context): MetronomeViewModelFactory {
        val minBpm = context.resources.getInteger(R.integer.min_bpm)
        val maxBpm = context.resources.getInteger(R.integer.max_bpm)
        return MetronomeViewModelFactory(minBpm, maxBpm)
    }
}