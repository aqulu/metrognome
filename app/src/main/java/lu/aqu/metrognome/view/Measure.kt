package lu.aqu.metrognome.view

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.LinearLayout
import lu.aqu.metrognome.R

class Measure : LinearLayout {

    private var mBeats: List<Beat> = emptyList()

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.Measure, 0, 0)
        val beats: Int
        try {
            beats = a.getInt(R.styleable.Measure_beats, 4)
        } finally {
            a.recycle()
        }

        orientation = HORIZONTAL
        setBeats(beats)
    }

    /**
     * sets the number of beats to be displayed by this measure
     * @param beats the number of beats to display
     */
    fun setBeats(beats: Int) {
        weightSum = beats.toFloat()

        val count = mBeats.count()
        when {
            beats < count -> mBeats -= removeBeats(count - beats)
            beats > count -> mBeats += addBeats(beats - count)
        }
    }

    /**
     * adds the specified number of beats to the current measure
     * @param count number of beats to add
     * @return List<Beat> list of newly added beats
     */
    private fun addBeats(count: Int): List<Beat> {
        val newBeats = (1..count).map {
            Beat(context)
        }

        newBeats.forEach { beat ->
            val layoutParams = LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f)
            addView(beat, layoutParams)
        }

        return newBeats
    }

    /**
     * drops the specified number of beats from the current measure
     * @param count number of beats to remove
     * @return List<Beat> list of removed beats
     */
    private fun removeBeats(count: Int): List<Beat> {
        val removable = mBeats.takeLast(count)
        removable.forEach(this::removeView)
        return removable
    }

    /**
     * updates the beats display so all beats up to <code>step</code> are displayed as active / played
     * @param step  number / index of the upper most active beat
     */
    fun setStep(step: Int) {
        mBeats.forEachIndexed { index, beat ->
            beat.setActive(index + 1 <= step)
        }
    }

    private class Beat(context: Context) : ImageView(context) {
        init {
            setActive(false)
        }

        fun setActive(isActive: Boolean) {
            val resource = if (isActive) {
                R.drawable.ic_circle_full_black_24dp
            } else {
                R.drawable.ic_circle_empty_black_24dp
            }
            setImageResource(resource)
        }
    }
}
